const searchPageA = 'searchPageA';
const searchPageB = 'searchPageB';
const searchPageFinal = 'searchPageFinal';
const detailsPage = 'detailsPage';
const AppScreen = 'AppScreen';

export {
    searchPageA,
    searchPageB,
    searchPageFinal,
    detailsPage,
    AppScreen,
}