/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Dimensions,
    SectionList,
    TouchableOpacity,
    TouchableWithoutFeedback,
    AsyncStorage,
} from 'react-native';
import {
    Icon
} from 'native-base';
import {
    StackNavigator
} from 'react-navigation'
import {Divider} from 'react-native-elements';
import AtoZ from './classes/AtoZPage/AtoZPage';
import {searchPageFinal} from "./screenNames";


const ITEM_HEIGHT = 50;
const _ = require('lodash');
let WIDTH = Dimensions.get('window').width;
let HEIGHT = Dimensions.get('window').height;

type Props = {};

export default class App extends Component<Props> {

    constructor(props, context) {
        super(props, context);

        let nameData = [
            {nama   : 'aza', id: 'amani', params: ''},
            {nama   : 'bobi', id: 'ok', params: '123'},
            {nama   : 'caca'},
            {nama   : 'devi'},
            {nama   : 'evan'},
            {nama   : 'feli'},
            {nama   : 'gohan'},
            {nama   : 'haha'},
            {nama   : 'ivan'},
            {nama   : 'johan'},
            {nama   : 'kaleb'},
            {nama   : 'lemon'},
        ];

        var data = [
            {data: [], key: 'A'},
            {data: [], key: 'B'},
            {data: [], key: 'C'},
            {data: [], key: 'D'},
            {data: [], key: 'E'},
            {data: [], key: 'F'},
            {data: [], key: 'G'},
            {data: [], key: 'H'},
            {data: [], key: 'I'},
            {data: [], key: 'J'},
            {data: [], key: 'K'},
            {data: [], key: 'L'},
            {data: [], key: 'M'},
            {data: [], key: 'N'},
            {data: [], key: 'O'},
            {data: [], key: 'P'},
            {data: [], key: 'Q'},
            {data: [], key: 'R'},
            {data: [], key: 'S'},
            {data: [], key: 'T'},
            {data: [], key: 'U'},
            {data: [], key: 'V'},
            {data: [], key: 'W'},
            {data: [], key: 'X'},
            {data: [], key: 'Y'},
            {data: [], key: 'Z'},
            {data: [], key: '#'},
        ];

        this.state = {
            obatArray: nameData,
            dataArray: data,
        };

    }


    filterData() {
        var data          = _.cloneDeep(this.state.dataArray);
        this.state.obatArray.map((item, index) => {
            for (let i = 0; i < data.length; i++) {
                if (i == data.length - 1) {
                    data[i].data.push(item);
                    break
                } else if (data[i].key == item.nama.charAt(0).toUpperCase()) {
                    data[i].data.push(item);
                    break
                } else {
                    continue
                }
            }
        });
        let delData       = [];
        let letterData    = [];
        for (var i in data) {
            if (data[i].data.length != 0) {
                delData.push(data[i])
                letterData.push(data[i].key)
            }
        }
        return {
            delData       : delData,
            letterData    : letterData
        }
    }

    sectionItem(item, index) {
        return (
            <View style={{
                // marginTop: 10,
                paddingTop: 10,
                paddingBottom: 10,
                // borderTopLeftRadius: 15,
                // borderTopRightRadius: 15,
                // borderBottomLeftRadius: 15,
                // borderBottomRightRadius: 15,
                // backgroundColor: '#FFFFFF',
                elevation: 2,
                marginBottom: 5

            }}>
                <TouchableOpacity onPress={() => alert(item.nama + index)}>
                    <Text
                        style={{
                            fontFamily    : 'Nunito-Regular',
                            fontSize      : 16,
                            color         : '#454F63',
                            marginLeft    : 10,
                            marginRight   : 5,
                        }}>{item.nama}</Text>
                    <Text
                        style={{
                            fontFamily    : 'Nunito-Regular',
                            fontSize      : 12,
                            color         : '#959DAD',
                            marginLeft    : 10,
                            marginRight   : 5,
                        }}>Test</Text>
                    <Divider style={{backgroundColor: '#EAF4FF', marginLeft: 10, marginRight: 10}}/>
                </TouchableOpacity>
            </View>
        )
    }

    alphabetLetter(letterData) {
        return (
            letterData.map((item, index) => {
                let otherStyle = [];
                if (index == letterData.length - 1) {
                    if (item == '#') {
                        otherStyle.push({width: 20})
                    }
                }
                return (
                    <TouchableWithoutFeedback key={'letter_' + index} onPress={() => {
                        this.sectionList.scrollToLocation({
                            animated: true,
                            itemIndex: 0,
                            sectionIndex: index,
                            viewOffset: (1 * (index + 0)) + (1 * index)
                        })
                    }}>
                        <View style={[styles.letterItemView]}>
                            <Text numberOfLines={0}
                                  style={[styles.letterText]}>{item}</Text>
                        </View>
                    </TouchableWithoutFeedback>
                )
            })
        )
    }


    render() {
        let filterData = this.filterData();
        let delData = filterData.delData;
        let letterData = filterData.letterData;
        const {navigation} = this.props;

        return (
            <View style={{flex: 1}}>
                <View style={styles.background}>
                    <View style={styles.top}>

                        <View style={styles.topMargin}>
                            <TouchableWithoutFeedback onPress={() => {
                                navigation.navigate(searchPageFinal,{obatArray: this.state.obatArray});}}>
                                <Icon style={styles.search_button} name='ios-search'/>
                            </TouchableWithoutFeedback>
                            <Text style={styles.welcome}>A to Z</Text>
                            <Text style={styles.atoz50Opacity}>A to Z</Text>
                            <Text style={styles.instructions}>Rangkuman informasi obat baik generik</Text>
                            <Text style={styles.instructions}>maupun brand disusun secara alphabet</Text>

                        </View>


                        <View style={styles.scroll_container}>
                            <Text style={styles.scroll_title}>Alfabet</Text>
                            <View style={{height: 70, marginTop: 10, marginLeft: 5, width: WIDTH - 6}}>

                                <ScrollView style={styles.horizontalScroll} scrollEventThrottle={26} horizontal={true}
                                            showsHorizontalScrollIndicator={false}>
                                    <AtoZ scrollLetter    = 'A' item='abothil'/>
                                    <AtoZ scrollLetter    = 'B'/>
                                    <AtoZ scrollLetter    = 'C'/>
                                    <AtoZ scrollLetter    = 'D'/>
                                    <AtoZ scrollLetter    = 'E'/>
                                    <AtoZ scrollLetter    = 'F'/>
                                    <AtoZ scrollLetter    = 'G'/>
                                    <AtoZ scrollLetter    = 'H'/>
                                    <AtoZ scrollLetter    = 'I'/>
                                    <AtoZ scrollLetter    = 'J'/>
                                    <AtoZ scrollLetter    = 'K'/>
                                    <AtoZ scrollLetter    = 'L'/>
                                    <AtoZ scrollLetter    = 'M'/>
                                    <AtoZ scrollLetter    = 'N'/>
                                    <AtoZ scrollLetter    = 'O'/>
                                    <AtoZ scrollLetter    = 'P'/>
                                    <AtoZ scrollLetter    = 'Q'/>
                                    <AtoZ scrollLetter    = 'R'/>
                                    <AtoZ scrollLetter    = 'S'/>
                                    <AtoZ scrollLetter    = 'T'/>
                                    <AtoZ scrollLetter    = 'U'/>
                                    <AtoZ scrollLetter    = 'V'/>
                                    <AtoZ scrollLetter    = 'W'/>
                                    <AtoZ scrollLetter    = 'X'/>
                                    <AtoZ scrollLetter    = 'Y'/>
                                    <AtoZ scrollLetter    = 'Z'/>
                                </ScrollView>

                            </View>
                        </View>
                    </View>

                    <View style={styles.listObat}>

                        <SectionList
                            data                            = {this.state.obatArray}
                            ref                             = {s => this.sectionList = s}
                            sections                        = {delData}
                            keyExtractor                    = {(item, index) => item + index}
                            renderItem                      = {({ item, index }) => this.sectionItem(item, index)}
                            // getItemLayout={(data, index) => ( {length: 50, offset: 50 * index, index} )}
                            initialNumToRender              = {this.state.obatArray.length}
                            showsVerticalScrollIndicator    = {false}
                        />

                    </View>
                    <View style={[styles.letterView]}>
                        {
                            this.alphabetLetter(letterData)
                        }
                    </View>
                </View>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    welcome: {
        fontSize: 30,
        textAlign: 'left',
        marginLeft: 10,
        marginTop: 10,
        marginBottom: 25,
        color: '#FFFFFF',
        fontFamily: 'Nunito-Bold',
    },
    instructions: {
        textAlign: 'left',
        color: '#FFFFFF',
        fontFamily: 'Nunito-Regular',
        fontSize: 12,
        marginLeft: 10,
    },
    topMargin: {
        marginLeft: 10,
        width: WIDTH,
        alignItems: 'flex-start'
    },
    top: {
        backgroundColor: '#CB1D50',
        height: HEIGHT * 0.6,
        width: WIDTH * 2,
        borderBottomLeftRadius: WIDTH * 4,
        borderBottomRightRadius: WIDTH * 4,
        flexDirection: 'column',
        alignItems: 'center'
        // borderRadius: WIDTH/4,
    },
    background: {
        flex: 1,
        backgroundColor: '#F3F3F3',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    search_button: {
        color: 'white',
        alignItems: 'flex-end',
        position: 'absolute',
        right: 0,
        marginRight: 20,
        marginTop: 20,

    },
    scroll_container: {
        marginTop: 10,
        elevation: 3,
        width: WIDTH,
        // borderColor : ''
    },
    scroll_letter: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 16,
        color: '#CE1F54',
        textAlign: 'center'
    },
    scroll_title: {
        fontFamily: 'Nunito-Regular',
        fontSize: 13,
        color: '#FFFFFF',
        marginLeft: 15,
    },
    horizontalScroll: {
        backgroundColor: '#EE2668',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10,
        paddingTop: 10,

    },
    atoz50Opacity: {
        position: 'absolute',
        opacity: 0.2,
        textAlign: 'left',
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Nunito-Bold',
        marginLeft: 30,

    },
    listObat: {
        backgroundColor: '#FFFFFF',
        borderTopRightRadius: 15,
        borderTopLeftRadius: 15,
        // borderBottomLeftRadius: 15,
        // borderBottomRightRadius: 15,
        flex: 1,
        width: WIDTH * 0.85,
        marginTop: -(HEIGHT / 5),
        // elevation: 2,
    },
    letterView: {
        width: 40,
        position: 'absolute',
        alignItems: 'flex-end',
        marginTop: HEIGHT*0.55,
        marginRight: 5,
        flex: 1,
        right: 0,
    },
    letterItemView: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 0.2,
        paddingHorizontal: 2,
    },
    letterText: {
        fontSize: 11,
        color: '#333333',
        fontFamily: 'SFProText-SemiBold'
    }
});
