/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native';



const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});
type Props = {};

export default class AtoZPage extends Component<Props> {

    GetItem = (item) => {
        alert(item);
    };

    render() {
        return (
            <View style={{
                height: 50,
                width: 50,
                borderRadius: 50 / 2,
                backgroundColor: '#FFFFFF',
                marginLeft: 10,
                justifyContent: 'center',
                alignItems: 'center',
                elevation: 5

            }}>
                <TouchableOpacity onPress = {() => this.GetItem(this.props.item)}>
                    <Text style={styles.scroll_letter}>{this.props.scrollLetter}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    scroll_container: {
        marginTop: 10,
        // borderColor : ''
    },
    topMargin: {
        marginLeft: 10,

    },
    top: {
        backgroundColor: '#CB1D50',
        height: 300,
        width: 'auto',
        borderBottomEndRadius: 150 / 2,
        borderBottomStartRadius: 150 / 2,

    },
    background: {
        flex: 1,
        backgroundColor: '#F3F3F3',
    },
    atoz50: {
        position: 'absolute',
        opacity: 0.2,
        textAlign: 'left',
        color: '#FFFFFF',
        fontSize: 80,
        fontFamily: 'Nunito-Bold',
        marginLeft: 30,

    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 30,
        textAlign: 'left',
        marginLeft: 10,
        marginTop: 20,
        marginBottom: 25,
        color: '#FFFFFF',
        fontFamily: 'Nunito-Bold',
    },
    instructions: {
        textAlign: 'left',
        color: '#FFFFFF',
        fontFamily: 'Nunito-Regular',
        fontSize: 12,
        marginLeft: 10,
    },
    scroll_letter: {
        fontFamily: 'Poppins-SemiBold',
        fontSize: 16,
        color: '#CE1F54',
        textAlign: 'center'
    },
    scroll_title: {
        fontFamily: 'Nunito-Regular',
        fontSize: 13,
        color: '#FFFFFF',
        marginLeft: 10,
    },
    horizontalScroll: {
        backgroundColor: '#EE2668',
        borderRadius: 10,
        padding: 10,

    }
});
