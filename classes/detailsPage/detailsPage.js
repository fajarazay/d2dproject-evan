import React, {Component} from 'react';
import ViewMoreText from 'react-native-view-more-text';
import {
    ListView,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback, TouchableHighlight,
    View,
    TextInput,
    FlatList,
    Dimensions,
    ScrollView,
} from 'react-native';
import {Icon, Button} from 'native-base';
import {searchPageFinal} from "../../screenNames";

const WIDTH    = Dimensions.get('window').width;
const HEIGHT   = Dimensions.get('window').height;

type Props = {};

class detailsPage extends Component<Props>{

    constructor(props){
        super(props);

        // this.scrollView = React.createRef();

        this.state = {
            activeTab   : 1,
            yIndex      : 0
        }
    }

    scrollInto = (index) => {
        let number = ((index == 2 ? 185 : (index == 3 ? 325 : 0)))
        this.scrollView.scrollTo({x:0,y:(number),animated:true})
    }

    onTabPress = (index) => {
        this.setState({activeTab: index, checked: []});
        this.scrollInto(index);
        // setTimeout(() => {
        //     this.onFilterData(this.state.search);
        // }, 500);
    }

    renderViewMore(onPress){
        return(
          <Text
          style={{fontFamily:'Nunito-Bold',fontSize:14,marginLeft:15,marginBottom:15}} 
          onPress={onPress}>View more</Text>
        )
      }

    renderViewLess(onPress){
        return(
          <Text
          style={{fontFamily:'Nunito-Bold',fontSize:14,marginLeft:15,marginBottom:15}}
          onPress={onPress}>View less</Text>
        )
    }

    render(){
        const {navigation}    = this.props;
        const { params }      = this.props.navigation.state;
        const name            = params ? params.name : null;
        const article         = params ? params.artikel : null;
        const search         = params ? params.search : null;


        return (
            <View style={{flex : 1}}>
                <View style={styles.background}>
                    <View style={styles.top}>

                        <View style={styles.topMargin}>
                            <TouchableWithoutFeedback onPress={() => {
                                    navigation.navigate(searchPageFinal,{search:search})}}>
                                    <Icon style={styles.back_button} name='md-arrow-round-back'/>
                            </TouchableWithoutFeedback>
                            

                            <Text style={styles.nama_obat}>{name}</Text>
                        </View>

                        <View style={{
                            flexDirection   : 'row',
                            width           : WIDTH,
                            marginTop       : HEIGHT*0.05}}>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(1)}
                                // onPress={()=>this._onPress(1,'semua',search)}
                                active={this.state.activeTab == 1}
                                style={{
                                    flex                : 1,
                                    borderRightWidth    : 0.3,
                                    borderRightColor    : 'rgba(244, 244, 246, 0.5)',
                                    borderBottomWidth   : this.state.activeTab == 1 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 1 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 1 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>PENGGUNAAN</Text>
                            </Button>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(2)}
                                // onPress={()=>this._onPress(2,'generik',search)}
                                active={this.state.activeTab == 2}
                                style={{
                                    flex                : 1,
                                    borderRightWidth    : 0.3,
                                    borderRightColor    : 'rgba(244, 244, 246, 0.5)',
                                    borderBottomWidth   : this.state.activeTab == 2 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 2 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 2 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>CARA KERJA</Text>
                            </Button>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(3)}
                                // onPress={()=>this._onPress(3,'brand',search)}
                                active={this.state.activeTab == 3}
                                style={{
                                    flex                : 1,
                                    borderBottomWidth   : this.state.activeTab == 3 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 3 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 3 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>PERINGATAN</Text>
                            </Button>
                        </View>
                    </View>

                    <ScrollView 
                        ref={s => this.scrollView = s}
                        showsVerticalScrollIndicator={false}
                        style={{
                            width           : WIDTH*0.95,
                            borderRadius    : 15,
                            backgroundColor : '#FFFFFF',
                            marginTop: -(HEIGHT / 2.7),
                            marginBottom: 16,
                    }}>

                        <Text style={styles.nama_obat2}>{name}</Text>

                        <View>
                            <Text style={styles.sub_title}>Penggunaan</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        {/* <Text style={styles.text}>{article}</Text> */}

                        <View>
                            <Text style={styles.sub_title}>Cara Kerja</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        <View>
                            <Text style={styles.sub_title}>Peringatan</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        <View>
                            <Text style={styles.sub_title}>Interaksi</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        <View>
                            <Text style={styles.sub_title}>Dosis</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        <View>
                            <Text style={styles.sub_title}>Kontradiksi</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>

                        <View>
                            <Text style={styles.sub_title}>Nama Paten</Text>

                            <ViewMoreText
                                numberOfLines={3}
                                renderViewMore={this.renderViewMore}
                                renderViewLess={this.renderViewLess}
                                textStyle={styles.text}
                            >
                                <Text>{article}</Text>
                            </ViewMoreText>
                        </View>
                    </ScrollView>


                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex                      : 1,
        backgroundColor           : '#F3F3F3',
        alignItems                : 'center',
    },
    top: {
        backgroundColor           : '#CB1D50',
        height                    : HEIGHT * 0.6,
        width                     : WIDTH * 2,
        borderBottomLeftRadius    : WIDTH * 4,
        borderBottomRightRadius   : WIDTH * 4,
        flexDirection             : 'column',
        alignItems                : 'center'
        // borderRadius: WIDTH/4,
    },
    topMargin: {
        marginLeft                : 0,
        width                     : WIDTH,
        // alignItems: 'flex-start'
    },
    back_button: {
        color                     : 'white',
        alignItems                : 'flex-start',
        position                  : 'absolute',
        left                      : 0,
        marginLeft                : 20,
        marginTop                 : 20,
    },
    nama_obat: {
        alignSelf                 : 'center',
        justifyContent            : 'center',
        marginTop                 : 20,
        fontFamily                : 'Nunito-SemiBold',
        fontSize                  : 18,
        color                     : '#FFFFFF'
    },
    nama_obat2: {
        alignSelf                 : 'center',
        justifyContent            : 'center',
        marginTop                 : 10,
        fontFamily                : 'Nunito-Bold',
        fontSize                  : 20,
        color                     : '#454F63'
    },
    sub_title: {
        alignSelf                 : 'flex-start',
        justifyContent            : 'center',
        marginTop                 : 10,
        marginLeft                : 15,
        fontFamily                : 'Nunito-Bold',
        fontSize                  : 16,
        color                     : '#454F63'
    },
    text : {
        alignSelf                 : 'flex-start',
        justifyContent            : 'center',
        marginTop                 : 10,
        marginLeft                : 15,
        marginRight               : 15,
        fontFamily                : 'Nunito-Regular',
        fontSize                  : 14,
        color                     : '#78849E'
    }

})

export default detailsPage;