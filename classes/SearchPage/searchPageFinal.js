import Autocomplete from 'react-native-autocomplete-input';
import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback,
    View,
    FlatList,
    Dimensions,
    AsyncStorage,
    SectionList
} from 'react-native';
import {SearchBar, Divider} from 'react-native-elements';
import {Icon} from 'native-base';
import {detailsPage,AppScreen} from "../../screenNames";


const WIDTH       = Dimensions.get('window').width;
const HEIGHT      = Dimensions.get('window').height;
const _           = require('lodash');

type Props = {};

class searchPageFinal extends Component<Props> {

    constructor() {
        super();

        this.state = {
            isLoading   : true,
            dataArray   : [],
            checked     : [],
            dataMounted : false,
            search      : '',
        };

        let data = [
            {data   : [], key: 'A'},
            {data   : [], key: 'B'},
            {data   : [], key: 'C'},
            {data   : [], key: 'D'},
            {data   : [], key: 'E'},
            {data   : [], key: 'F'},
            {data   : [], key: 'G'},
            {data   : [], key: 'H'},
            {data   : [], key: 'I'},
            {data   : [], key: 'J'},
            {data   : [], key: 'K'},
            {data   : [], key: 'L'},
            {data   : [], key: 'M'},
            {data   : [], key: 'N'},
            {data   : [], key: 'O'},
            {data   : [], key: 'P'},
            {data   : [], key: 'Q'},
            {data   : [], key: 'R'},
            {data   : [], key: 'S'},
            {data   : [], key: 'T'},
            {data   : [], key: 'U'},
            {data   : [], key: 'V'},
            {data   : [], key: 'W'},
            {data   : [], key: 'X'},
            {data   : [], key: 'Y'},
            {data   : [], key: 'Z'},
            {data   : [], key: '#'},
        ];
    }

    static defaultProps = {
        nameData: [
            {nama   : 'aza'          , id: 'amani',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'bobi'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'caca'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'devi'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'evan'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'feli'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohan'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'haha'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivan'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'johan'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kaleb'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lemon'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'assaf'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'dbfbre'       , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'everbereran'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'erbrbrtb'     , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohrbeberan'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'harberberha'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivdbdfvsdan'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'jorebervdhan' , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kalsdvew eb'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lesdvevervmon', id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
        ]
    }

    componentDidMount() {
        for (let i = 0; i < this.props.nameData.length; i++) {
            this.state.dataArray.push(this.props.nameData[i]);
        }
        
        // AsyncStorage.getItem("searchValue").then((search) =>{
        //     this.setState({search:search})
        // })
    }

    filterArray = (text) => {
        let search = text.toLowerCase();
        if (search != '') {
            this.setState({
                checked: this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(search)),
                search,
            });
            // AsyncStorage.setItem("searchValue",search);
        } else {
            this.state.checked = []
        }
        this.setState({
            search
        });
        // console.log(this.state.checked);
    };

    _renderItem = (item, index, section) => {
        // console.log('---custom-renderItem--', item, index, section);
        console.log('item', item.item);
        const{navigation} = this.props;
        return (
            <TouchableOpacity onPress={() => {
                navigation.navigate(detailsPage,{name:item.item.nama,artikel:item.item.artikel})
                console.log(item.item.nama)
            }}>
                <View style={{
                    marginTop       : 10,
                    marginBottom    : 10,
                    height          : HEIGHT*0.085,
                    // width           : WIDTH* 0.81,
                    // justifyContent: 'center',
                    flex            : 1,
                    // paddingBottom:30,
                    // paddingTop:30,

                }}>
                    <Text 
                        style={{
                            fontFamily    : 'Nunito-SemiBold',
                            fontSize      : 16               ,
                            color         : '#6C6C6C'        ,
                            marginLeft    : 20               ,
                            marginBottom  : 5               ,
                            // marginTop:5
                        }}>{item.item.nama}</Text>
                    <Text
                        style={{
                            fontFamily    : 'Nunito-Regular',
                            fontSize      : 12,
                            color         : '#959DAD',
                            marginLeft    : 20,
                            // marginRight: 15,
                            marginBottom  : 15
                        }}>Test</Text>

                    <Divider style={{backgroundColor: '#EEF0F5', marginLeft: 10, marginRight: 10}}/>
                </View>
            </TouchableOpacity>
        );
    };

    updateSearch = search => {
        this.setState({search});
    };

    noUnderLine() {
        return (null);
    }


    render() {
        const {search}        = this.state;
        const {navigation}    = this.props;
        const { params }      = this.props.navigation.state;
        const savedSearch     = params ? params.search : null;
        let sortedChecked     = this.state.checked.sort((a, b) => (a.nama > b.nama) ? 1 : -1);

        // ! setelah icon di detailsPage ditekan
        // if  (savedSearch != null){
        //     this.setState({
        //         checked: this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(savedSearch)),
        //     });
        // }

        return (
            <View style={{
                // flex: 1,
                // justifyContent          : 'center',
                alignItems              : 'center',
                }}>
                <View style={{width: WIDTH,backgroundColor:'#CB1D50',alignItems: 'center'}}>

                    <TouchableWithoutFeedback onPress={() => {
                        navigation.navigate(AppScreen)}}>
                        <Icon style={styles.back_button} name='md-arrow-round-back'/>
                    </TouchableWithoutFeedback>

                    <Text style={{marginTop: 20,color: '#FFFFFF', fontSize: 18, fontFamily: 'Nunito-SemiBold'}}>List Generic A</Text>

                    <SearchBar
                        placeholder        =""
                        onChangeText       ={this.filterArray}
                        value              ={search}
                        round              ={true}
                        lightTheme         ={true}
                        containerStyle     ={styles.autocompleteContainer}
                        inputContainerStyle={styles.container}
                        inputStyle         ={styles.inputText}
                        onClear            ={this.empty}
                    />
                </View>
                <View style={{
                        // justifyContent          : 'center',
                        // alignItems                          : 'center',
                        backgroundColor                     : '#FFFFFF',
                        position                            : 'relative',
                        height                              : HEIGHT*0.73,
                        borderTopRightRadius                : 15,
                        borderTopLeftRadius                 : 15,
                        // borderBottomLeftRadius  : 15,
                        // borderBottomRightRadius : 15,
                        marginTop                           : 15,
                        width                               : WIDTH * 0.9,
                        elevation                           : 1,
                    }}>
                        <FlatList
                            showsVerticalScrollIndicator    = {false}
                            data                            = {sortedChecked}
                            renderItem                      = {this._renderItem}
                            ItemSeparatorComponent          = {this.noUnderLine }
                            // keyExtractor={item => item.nama}
                        />
                        {/* <SectionList
                            data                            = {this.state.checked}
                            sections                        = {delData}
                            keyExtractor                    = {(item, index) => item + index}
                            renderItem                      = {({ item }) => this._renderItem(item)}
                            initialNumToRender              = {this.state.checked.length}
                            showsVerticalScrollIndicator    = {false}
                        /> */}
                    </View>
            </View>
        );
    }
}

const border = {
    borderColor   : '#b9b9b9',
    borderRadius  : 1,
    // borderWidth: 1
};

const androidStyles = {
    // container: {
    //   flex: 1
    // },
    inputContainer: {
        ...border,
        marginBottom    : 0
    },
    list: {
        ...border,
        backgroundColor : 'white',
        borderTopWidth  : 0,
        margin          : 10,
        marginTop       : 0
    }
};

const iosStyles = {
    // container: {
    //   zIndex: 1
    // },
    inputContainer: {
        ...border
    },
    input: {
        backgroundColor   : 'white',
        height            : 40,
        paddingLeft       : 3
    },
    list: {
        ...border,
        backgroundColor   : 'white',
        borderTopWidth    : 0,
        left              : 0,
        position          : 'absolute',
        right             : 0
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor         : '#FFFFFF',
        // flex: 1,
        // paddingTop: 25
        width                   : 0.95*WIDTH,
        height                  : 35,
    },
    autocompleteContainer: {
        // flex: 1,
        left                    : 0,
        position                : 'relative',
        right                   : 0,
        // zIndex: 1,
        // elevation: 4,
        backgroundColor         : 'transparent',
        height                  : 76,
        justifyContent          : 'center',
        borderBottomColor       : 'transparent',
        borderTopColor          : 'transparent'
    },
    inputText: {
        fontFamily              : 'Nunito-Regular',
        fontSize                : 14,
        color                   : '#6C6C6C'
    },
    cancelButton: {
        position                : 'absolute',
        alignItems              : 'flex-end',
        color                   : 'white',
        zIndex                  : 2,
        right                   : 0,
        marginRight             : 23,
        marginTop               : 23,
    },
    itemText: {
        fontSize                : 15,
        margin                  : 2
    },
    infoText: {
        textAlign               : 'center'
    },
    titleText: {
        fontSize                : 18,
        fontWeight              : '500',
        marginBottom            : 10,
        marginTop               : 10,
        textAlign               : 'center'
    },
    directorText: {
        color                   : 'grey',
        fontSize                : 12,
        marginBottom            : 10,
        textAlign               : 'center'
    },
    openingText: {
        textAlign               : 'center'
    },
    input: {
        backgroundColor         : 'white',
        height                  : 40,
        paddingLeft             : 3
    },
    listObat: {
        backgroundColor         : '#FFFFFF',
        borderTopRightRadius    : 15,
        borderTopLeftRadius     : 15,
        // borderBottomLeftRadius: 15,
        // borderBottomRightRadius: 15,
        flex                    : 1,
        width                   : WIDTH * 0.85,
        marginTop               : -(HEIGHT / 5),
        // elevation: 2,
    },
    back_button: {
        color                   : 'white',
        alignItems              : 'flex-start',
        position                : 'absolute',
        left                    : 0,
        marginLeft              : 20,
        marginTop               : 20,
    },
    // ...Platform.select({
    //     android: {...androidStyles},
    //     ios: {...iosStyles}
    // })
});

export default searchPageFinal;