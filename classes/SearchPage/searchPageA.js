import Autocomplete from 'react-native-autocomplete-input';
import React, {Component} from 'react';
import {
    ListView,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback,
    View,
    TextInput,
    FlatList,
    Dimensions
} from 'react-native';
import {SearchBar, Divider} from 'react-native-elements';
import {Icon} from 'native-base';
import {searchPageB,searchPageFinal,detailsPage} from "../../screenNames";


const ITEM_WIDTH = Dimensions.get('window').width;
const ITEM_HEIGHT = Dimensions.get('window').height;

type Props = {};

class searchPageA extends Component<Props> {

    constructor() {
        super();

        this.state = {
            isLoading  : true,
            dataArray  : [],
            checked    : [],
            dataMounted: false,
        }
    }

    static defaultProps = {
        nameData: [
            {nama   : 'aza'          , id: 'amani',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'bobi'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'caca'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'devi'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'evan'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'feli'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohan'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'haha'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivan'         , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'johan'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kaleb'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lemon'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'assaf'        , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'dbfbre'       , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'everbereran'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'erbrbrtb'     , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'gohrbeberan'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'harberberha'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'ivdbdfvsdan'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'jorebervdhan' , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'kalsdvew eb'  , id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
            {nama   : 'lesdvevervmon', id: 'ok',artikel: 'Artikel adalah karangan faktual secara lengkap dengan panjang tertentu yang dibuat untuk dipublikasikan di media online maupun cetak (melalui koran, majalah, buletin, dsb) dan bertujuan menyampaikan gagasan dan fakta yang dapat meyakinkan, mendidik, dan menghibur.'},
        ]
    }

    componentDidMount() {
        for (let i = 0; i < this.props.nameData.length; i++) {
            this.state.dataArray.push(this.props.nameData[i]);
        }
        // this.state.dataMounted = true;
    }

    filterArray = (text) => {
        let search = text.toLowerCase();
        if (search != '') {
            this.setState({
                checked: this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(search)),
                search
            });
        } else {
            this.state.checked = []
        }
        this.setState({
            search
        });
        // console.log(this.state.checked);
    };

    _renderItem = (item, index, section) => {
        // console.log('---custom-renderItem--', item, index, section);
        console.log('item', item.item);
        const{navigation} = this.props;
        return (
            <TouchableOpacity onPress={() => {
                navigation.navigate(detailsPage,{name:item.item.nama,artikel:item.item.artikel})
                console.log(item.item.nama)
            }}>
                <View style={{
                    marginTop   : 10,
                    height      : ITEM_HEIGHT*0.085,
                    width       : ITEM_WIDTH,
                    // justifyContent: 'center',
                    flex        : 1,
                    // paddingBottom:30,
                    // paddingTop:30,

                }}>
                    <Text 
                        style={{
                            fontFamily   : 'Nunito-SemiBold',
                            fontSize     : 14               ,
                            color        : '#6C6C6C'        ,
                            marginLeft   : 20               ,
                            marginBottom : 15               ,
                        }}>{item.item.nama}</Text>
                    <Text 
                        style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize: 14,
                            color: 'rgba(108, 108, 108, 0.7)',
                            marginRight: 20,
                            // alignItems: 'flex-end',
                            right: 0,
                            position: 'absolute'
                        }}>{item.item.id}</Text>

                    <Divider style={{backgroundColor: '#EEF0F5', marginLeft: 10, marginRight: 10}}/>
                </View>
            </TouchableOpacity>
        );
    };

    updateSearch = search => {
        this.setState({search});
    };

    noUnderLine() {
        return (null);
    }


    render() {
        const {search} = this.state;
        const {navigation} = this.props;
        

        return (
            <View style={{flex: 1}}>
                <View>
                    <SearchBar
                        placeholder        ="Cari Obat"
                        onChangeText       ={this.filterArray}
                        value              ={search}
                        round              ={true}
                        lightTheme         ={true}
                        containerStyle     ={styles.autocompleteContainer}
                        inputContainerStyle={styles.container}
                        inputStyle         ={styles.inputText}
                        onClear            ={this.empty}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        navigation.navigate(searchPageB),
                            console.log(this.state.checked),
                            this.empty;
                        this.state.checked
                    }}>
                        <Icon style={styles.cancelButton} name='close'/>
                    </TouchableWithoutFeedback>
                    <View style={{
                        backgroundColor: '#FFFFFF',
                        position       : 'relative',
                        height         : ITEM_HEIGHT*0.86,
                        // flex           : 1
                    }}>
                        <FlatList
                            data                   ={this.state.checked}
                            renderItem             ={this._renderItem }
                            ItemSeparatorComponent ={this.noUnderLine }
                            // keyExtractor={item => item.nama}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const border = {
    borderColor: '#b9b9b9',
    borderRadius: 1,
    borderWidth: 1
};

const androidStyles = {
    // container: {
    //   flex: 1
    // },
    inputContainer: {
        ...border,
        marginBottom: 0
    },
    list: {
        ...border,
        backgroundColor: 'white',
        borderTopWidth: 0,
        margin: 10,
        marginTop: 0
    }
};

const iosStyles = {
    // container: {
    //   zIndex: 1
    // },
    inputContainer: {
        ...border
    },
    input: {
        backgroundColor: 'white',
        height: 40,
        paddingLeft: 3
    },
    list: {
        ...border,
        backgroundColor: 'white',
        borderTopWidth: 0,
        left: 0,
        position: 'absolute',
        right: 0
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        // flex: 1,
        // paddingTop: 25
        width: 0.8*ITEM_WIDTH,
        height: 40
    },
    autocompleteContainer: {
        // flex: 1,
        left: 0,
        // position: 'absolute',
        right: 0,
        zIndex: 1,
        // elevation: 4,
        backgroundColor: '#CB1D50',
        height: 75,
        justifyContent: 'center',
    },
    inputText: {
        fontFamily: 'Nunito-Regular',
        fontSize: 14,
        color: '#6C6C6C'
    },
    cancelButton: {
        position: 'absolute',
        alignItems: 'flex-end',
        color: 'white',
        zIndex: 2,
        right: 0,
        marginRight: 23,
        marginTop: 23,
    },
    itemText: {
        fontSize: 15,
        margin: 2
    },
    descriptionContainer: {
        // `backgroundColor` needs to be set otherwise the
        // autocomplete input will disappear on text input.
        backgroundColor: '#F5FCFF',
        marginTop: 25
    },
    infoText: {
        textAlign: 'center'
    },
    titleText: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 10,
        marginTop: 10,
        textAlign: 'center'
    },
    directorText: {
        color: 'grey',
        fontSize: 12,
        marginBottom: 10,
        textAlign: 'center'
    },
    openingText: {
        textAlign: 'center'
    },
    input: {
        backgroundColor: 'white',
        height: 40,
        paddingLeft: 3
    },
    ...Platform.select({
        android: {...androidStyles},
        ios: {...iosStyles}
    })
});

export default searchPageA;