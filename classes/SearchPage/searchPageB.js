import Autocomplete from 'react-native-autocomplete-input';
import React, {Component} from 'react';
import {
    ListView,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity, TouchableWithoutFeedback, TouchableHighlight,
    View,
    TextInput,
    FlatList,
    Dimensions,
    ActivityIndicator
} from 'react-native';
import {SearchBar, Divider} from 'react-native-elements';
import {Icon, Button} from 'native-base';
import {searchPageA,searchPageFinal,detailsPage,AppScreen,} from "../../screenNames";

const ITEM_WIDTH = Dimensions.get('window').width;
const ITEM_HEIGHT = Dimensions.get('window').height;

type Props = {};

class searchPageB extends Component<Props> {

    constructor(props) {
        super(props);

        this.state = {
            isLoading       : true,
            dataArray       : [],
            dataMounted     : false,
            toggle          : true,
            semuaButton     : true,
            generikButton   : false,
            brandButton     : false,
            empty           : true,
            checked         : [],
            fChecked        : [],
            activeTab       : 1
        }
    }

    static defaultProps = {
        nameData: [
            {nama : 'aza'           , id: 'brand'  , params: ''    },
            {nama : 'bobi'          , id: 'generik' , params: '123'},
            {nama : 'caca'          , id: 'brand'                  },
            {nama : 'devi'          , id: 'brand'                  },
            {nama : 'evan'          , id: 'brand'                  },
            {nama : 'feli'          , id: 'brand'                  },
            {nama : 'gohan'         , id: 'generik'                },
            {nama : 'haha'          , id: 'generik'                },
            {nama : 'ivan'          , id: 'generik'                },
            {nama : 'johan'         , id: 'brand'                  },
            {nama : 'kaleb'         , id: 'generik'                },
            {nama : 'lemon'         , id: 'brand'                  },
            {nama : 'assaf'         , id: 'generik'                },
            {nama : 'dbfbre'        , id: 'generik'                },
            {nama : 'everbereran'   , id: 'brand'                  },
            {nama : 'erbrbrtb'      , id: 'generik'                },
            {nama : 'gohrbeberan'   , id: 'generik'                },
            {nama : 'harberberha'   , id: 'generik'                },
            {nama : 'ivdbdfvsdan'   , id: 'brand'                  },
            {nama : 'jorebervdhan'  , id: 'generik'                },
            {nama : 'kalsdvew eb'   , id: 'generik'                },
            {nama : 'lesdvevervmon' , id: 'brand'                  },

        ]
    }

    componentDidMount() {
        for (let i = 0; i < this.props.nameData.length; i++) {
            this.state.dataArray.push(this.props.nameData[i]);
        }
        // this.state.dataMounted = true;
    }

    // filterArray = (text) => {
    //     console.log('temp',this.state.temp)
    //     let search = text.toLowerCase();
    //     if (search != '') {
    //         this.setState({
    //             checked: this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(search)),
    //             fChecked: this.state.checked,
    //             search,
    //             empty: true
    //         })
    //     }
    //     else{
    //         this.setState({
    //             checked: [],
    //             fChecked: [],
    //             empty : true
    //         })
    //         console.log('checked is empty')
    //     }
    //     this.setState({
    //         search
    //     })
    // };

    _renderItem = (item) => {
        // console.log('---custom-renderItem--', item, index, section);
        // console.log('item', item.item);
        const {navigation} = this.props; 
        return (
            <TouchableOpacity onPress={() => {
                navigation.navigate(detailsPage,{name:item.item.nama})
                console.log(item.item.nama)
            }}>
                <View style={{
                    marginTop: 10,  
                    height:ITEM_HEIGHT*0.085,
                    width: ITEM_WIDTH,
                    // justifyContent: 'center',
                    flex: 1,
                    // paddingBottom:30,
                    // paddingTop:30,

                }}>
                    <Text 
                        style={{
                            fontFamily: 'Nunito-SemiBold',
                            fontSize  : 14,
                            color     : '#6C6C6C',
                            marginLeft: 20,
                            marginBottom: 15,
                        }}>{item.item.nama}{item.item.id}</Text>

                    <Divider style={{backgroundColor: '#EEF0F5', marginLeft: 10, marginRight: 10}}/>
                </View>
            </TouchableOpacity>
        );
    };

    updateSearch = search => {
        this.setState({search});
    };

    toggleBorder = function(){
        return{
            borderBottomWidth: 0.5,
            borderBottomColor: '#ffff59'
        }
    }

    onTabPress = (index) => {
        this.setState({activeTab: index, checked: []});
        setTimeout(() => {
            this.onFilterData(this.state.search);
        }, 500);
    }

    onFilterData = (keyword) => {
        let result = [];
        if (keyword) {
            keyword             = keyword.toLowerCase();

            let tab             = (this.state.activeTab == 2 ? 'generik' : (this.state.activeTab == 3 ? 'brand' : ''))
            let filterData      = this.state.dataArray.filter(obj => obj.nama.toLowerCase().includes(keyword));

            result              = filterData;
            // console.warn(tab)
            if (tab) {
                result          = result.filter(obj => obj.id == tab);
            }
        }

        this.setState({
            checked: result,
            search: keyword
        });
    }

    render() {
        const {search}            = this.state;
        const {navigation}        = this.props;
        const {toggle}            = this.state;

        return (
            <View style={{flex: 1}}>
                <View>
                    <SearchBar
                        placeholder           = "Cari Obat"
                        onChangeText          = {(txt) => this.onFilterData(txt)}
                        value                 = {search}
                        round                 = {true}
                        lightTheme            = {true}
                        containerStyle        = {styles.autocompleteContainer}
                        inputContainerStyle   = {styles.container}
                        inputStyle            = {styles.inputText}
                        onClear               = {this.empty}
                        underlineColorAndroid = "transparent"
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        navigation.navigate(searchPageFinal),
                            console.log(this.state.checked),
                            this.empty;
                        this.state.checked
                    }}>
                        <Icon style={styles.cancelButton} name='close'/>
                    </TouchableWithoutFeedback>
                    <View style={{
                        backgroundColor   : '#FFFFFF',
                        position          : 'relative',
                        height            : ITEM_HEIGHT*0.865,
                        // flex           : 1
                    }}>
                        <View style={{
                            flexDirection: 'row'}}>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(1)}
                                // onPress={()=>this._onPress(1,'semua',search)}
                                active={this.state.activeTab == 1}
                                style={{
                                    flex                : 1,
                                    borderRightWidth    : 0.3,
                                    borderRightColor    : 'rgba(244, 244, 246, 0.5)',
                                    borderBottomWidth   : this.state.activeTab == 1 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 1 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 1 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>SEMUA</Text>
                            </Button>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(2)}
                                // onPress={()=>this._onPress(2,'generik',search)}
                                active={this.state.activeTab == 2}
                                style={{
                                    flex                : 1,
                                    borderRightWidth    : 0.3,
                                    borderRightColor    : 'rgba(244, 244, 246, 0.5)',
                                    borderBottomWidth   : this.state.activeTab == 2 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 2 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 2 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>GENERIK</Text>
                            </Button>
                            
                            <Button full light
                                onPress={()=>this.onTabPress(3)}
                                // onPress={()=>this._onPress(3,'brand',search)}
                                active={this.state.activeTab == 3}
                                style={{
                                    flex                : 1,
                                    borderBottomWidth   : this.state.activeTab == 3 ? 6:0,
                                    borderBottomColor   : this.state.activeTab == 3 ? '#FFC107':''}}>
                                    <Text 
                                        style={{
                                            fontFamily    : 'Nunito-Bold',
                                            fontSize      : 13,
                                            color         : this.state.activeTab == 3 ? '#525C6E':'#959DAD',
                                            position      : 'absolute'}}>BRAND</Text>
                            </Button>
                            
                        </View>

                        <FlatList
                            data                      = {this.state.checked}
                            renderItem                = {this._renderItem}
                            ItemSeparatorComponent    = {this.noUnderLine }
                            // keyExtractor={item => item.nama}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const border = {
    borderColor: '#b9b9b9',
    borderRadius: 1,
    borderWidth: 1
};

const androidStyles = {
    // container: {
    //   flex: 1
    // },
    inputContainer: {
        ...border,
        marginBottom: 0
    },
    list: {
        ...border,
        backgroundColor: 'white',
        borderTopWidth: 0,
        margin: 10,
        marginTop: 0
    }
};

const iosStyles = {
    // container: {
    //   zIndex: 1
    // },
    inputContainer: {
        ...border
    },
    input: {
        backgroundColor: 'white',
        height: 40,
        paddingLeft: 3
    },
    list: {
        ...border,
        backgroundColor: 'white',
        borderTopWidth: 0,
        left: 0,
        position: 'absolute',
        right: 0
    }
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#FFFFFF',
        // flex: 1,
        // paddingTop: 25
        width: 0.8*ITEM_WIDTH,
        height: 40
    },
    autocompleteContainer: {
        // flex: 1,
        left: 0,
        // position: 'absolute',
        right: 0,
        zIndex: 1,
        // elevation: 4,
        backgroundColor: '#CB1D50',
        height: 75,
        justifyContent: 'center',
    },
    inputText: {
        fontFamily: 'Nunito-Regular',
        fontSize: 14,
        color: '#6C6C6C'
    },
    cancelButton: {
        position: 'absolute',
        alignItems: 'flex-end',
        color: 'white',
        zIndex: 2,
        right: 0,
        marginRight: 23,
        marginTop: 23,
    },
    itemText: {
        fontSize: 15,
        margin: 2
    },
    descriptionContainer: {
        // `backgroundColor` needs to be set otherwise the
        // autocomplete input will disappear on text input.
        backgroundColor: '#F5FCFF',
        marginTop: 25
    },
    infoText: {
        textAlign: 'center'
    },
    titleText: {
        fontSize: 18,
        fontWeight: '500',
        marginBottom: 10,
        marginTop: 10,
        textAlign: 'center'
    },
    directorText: {
        color: 'grey',
        fontSize: 12,
        marginBottom: 10,
        textAlign: 'center'
    },
    openingText: {
        textAlign: 'center'
    },
    input: {
        backgroundColor: 'white',
        height: 40,
        paddingLeft: 3
    },
    ...Platform.select({
        android: {...androidStyles},
        ios: {...iosStyles}
    })
});

export default searchPageB;