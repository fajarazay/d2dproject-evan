/**
 * @format
 */

import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import {StackNavigator} from 'react-navigation';


import searchPageA from './classes/SearchPage/searchPageA';
import searchPageB from './classes/SearchPage/searchPageB';
import searchPageFinal from './classes/SearchPage/searchPageFinal';

import detailsPage from './classes/detailsPage/detailsPage';

import AppPage from './App';
// import {searchPageA,AppScreen} from "./screenNames";

const App = StackNavigator({
    AppScreen:{
        screen: AppPage,
        navigationOptions: {
            header : null
        }
    },
    searchPageA:{
        screen: searchPageA,
        navigationOptions: {
            header : null
        }
    },
    searchPageB:{
        screen: searchPageB,
        navigationOptions: {
            header : null
        }
    },
    detailsPage:{
        screen: detailsPage,
        navigationOptions: {
            header : null
        }
    },
    searchPageFinal: {
        screen: searchPageFinal,
        navigationOptions: {
            header : null
        }
    }
});

AppRegistry.registerComponent(appName, () => App);
